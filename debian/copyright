Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Control package for Octave
Upstream-Contact: Lukas Reichlin <lukas.reichlin@gmail.com>
                  Alexander Wilms <f.alexander.wilms@gmail.com>
                  Doug Stewart <doug.dastew@gmail.com>
                  Torsten Lilge <ttl-octave@mailbox.org>
Source: https://gnu-octave.github.io/packages/control/

Files: *
Copyright: 2009-2016 Lukas F. Reichlin
           2015 Thomas D. Dean
           2009 Luca Favatella
           1997, 2000, 2004-2007 Kai P. Mueller
           2010 Benjamin Fernandez
           1996, 1998, 2000, 2002-2007 Auburn University
           2012 Megan Zagrobelny
           2011 Ferdinand Svaricek, UniBw Munich
           2013-2015 Thomas Vasileiou
           2017-2018 Fabian Alexander Wilms <f.alexander.wilms@gmail.com>
           2016-2017 Mark Bronsfeld
           2016 Douglas A. Stewart
           2017 Olaf Till <i7tiol@t-online.de>
           2019 Stefan Mátéfi-Tempfli
           2012-2020, 2022, 2023 Torsten Lilge
License: GPL-3+

Files: io.github.gnu_octave.pkg-control.metainfo.xml
Copyright: 2024 Torsten Lilge
License: FSFAP
 Copying and distribution of this file, with or without modification,
 are permitted in any medium without royalty provided the copyright
 notice and this notice are preserved.  This file is offered as-is,
 without any warranty.

Files: src/aclocal.m4
Copyright: 1996-2020, Free Software Foundation, Inc.
License: FSFULLR
 This file is free software; the Free Software Foundation gives
 unlimited permission to copy and/or distribute it, with or without
 modifications, as long as this notice is preserved.

Files: src/configure
Copyright: 1992-1996, 1998-2012, Free Software Foundation, Inc.
License: FSFUL
 This configure script is free software; the Free Software Foundation
 gives unlimited permission to copy, distribute and modify it.

Files: debian/*
Copyright: 2008 Olafur Jens Sigurdsson <ojsbug@gmail.com>
           2008, 2009, 2011-2012 Thomas Weber <tweber@debian.org>
           2009, 2012, 2015-2019, 2022, 2023 Rafael Laboissière <rafael@debian.org>
           2012-2025 Sébastien Villemot <sebastien@debian.org>
License: GPL-3+

Files: src/slicot/src/*
Copyright: 2020 SLICOT
License: BSD-3-clause

Files: src/src_aux/*
Copyright: 1992-2025 The University of Tennessee and The University of Tennessee Research Foundation
           2000-2025 The University of California Berkeley
           2006-2025 The University of Colorado Denver
License: BSD-3-clause

Files: src/TG04BX.f
Copyright: 2002-2009 NICONET e.V.
License: GPL-2+

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
 .
 3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from
    this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-2+
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2,
 or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License, version 2, can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License, version 3, can be found in the file
 `/usr/share/common-licenses/GPL-3'.
