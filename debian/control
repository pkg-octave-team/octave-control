Source: octave-control
Section: math
Priority: optional
Maintainer: Debian Octave Group <team+pkg-octave-team@tracker.debian.org>
Uploaders: Sébastien Villemot <sebastien@debian.org>,
           Rafael Laboissière <rafael@debian.org>,
           Mike Miller <mtmiller@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-octave (>= 1.2.3),
               dh-sequence-octave,
               libslicot-dev (>= 5.8)
Standards-Version: 4.7.2
Homepage: https://gnu-octave.github.io/packages/control/
Vcs-Git: https://salsa.debian.org/pkg-octave-team/octave-control.git
Vcs-Browser: https://salsa.debian.org/pkg-octave-team/octave-control
Testsuite: autopkgtest-pkg-octave
Rules-Requires-Root: no

Package: octave-control
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, ${octave:Depends}
Multi-Arch: same
Description: computer-aided control system design (CACSD) for Octave
 This package provides additional functions related to control theory
 in Octave, a numerical computation software. The functions are based on SLICOT
 Fortran library <http://www.slicot.org>.
 .
 This Octave add-on package is part of the Octave-Forge project.
